export default (app) => {
  app.get('/User', async (req, res) => {
    res.json([
      {
        name: 'Rafael',
        genre: 'M',
        age: 35
      },
      {
        name: 'Olívia',
        genre: 'F',
        age: 10
      }
    ])
  })
}