import express from 'express'
import path  from 'path'

import { IExpressUrlEncoded, } from '@crio/express/types'

import {
  EXPRESS_PORT,
  EXPRESS_CORS,
  EXPRESS_BODY,
  EXPRESS_SERVER_QUERYSTRING,
  EXPRESS_REQUEST_LIMIT,
  EXPRESS_PARAMETER_LIMIT,
  EXPRESS_STATIC_FOLDERS
} from '../../crio-config'

import routers from './routers'

export { value, } from '@crio/express'

export function start () {
  const app = express()
  const port = EXPRESS_PORT

  if (EXPRESS_CORS) {
    const cors = require('cors')
  
    app.use(cors())
  }

  if (EXPRESS_BODY) {
    app.use(express.json())
  }

  const urlencoded: IExpressUrlEncoded = {
    extended: EXPRESS_SERVER_QUERYSTRING,
    limit: EXPRESS_REQUEST_LIMIT,
    parameterLimit: EXPRESS_PARAMETER_LIMIT
  }

  app.use(express.urlencoded(urlencoded))

  EXPRESS_STATIC_FOLDERS.map(folder => {
    app.use(
      express.static(
        path.join('src', 'server', folder)
      )
    )
  })

  routers(app)
  
  app.listen(port, () => {
    console.log(`Listening at port ${port}`)
  })
}