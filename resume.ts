export const title: string = 'Express'
export const description: string = 'Install the Express module for CRIO'
export const value: string = '@crio/express'
export const moduleDir: string = __dirname