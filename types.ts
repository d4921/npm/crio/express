import { IInstallModuleParams, } from '@crio/main/types'

export interface IExpressCreateProjectResponse {
  serverPort: number,
  serverCors: boolean,
  serverBody: boolean,
  serverQueryString: boolean,
  serverRequestLimit: string,
  serverParameterLimit: number,
  serverStaticFolders: string[]
}

export interface IExpressConfig {
  EXPRESS_PORT: number,
  EXPRESS_CORS: boolean,
  EXPRESS_BODY: boolean,
  EXPRESS_SERVER_QUERYSTRING: boolean,
  EXPRESS_REQUEST_LIMIT: string,
  EXPRESS_PARAMETER_LIMIT: number,
  EXPRESS_STATIC_FOLDERS: string[]
}

export interface IExpressInstallProjectParams extends IInstallModuleParams {
  responses: IExpressCreateProjectResponse
}

export interface IExpressInstallProjectParams extends IInstallModuleParams {
  responses: IExpressCreateProjectResponse
}

export interface IExpressUrlEncoded {
  extended: boolean,
  limit: string,
  parameterLimit: number,
}