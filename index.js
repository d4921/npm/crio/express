"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.moduleDir = exports.value = exports.description = exports.title = void 0;
var resume_1 = require("./resume");
Object.defineProperty(exports, "title", { enumerable: true, get: function () { return resume_1.title; } });
Object.defineProperty(exports, "description", { enumerable: true, get: function () { return resume_1.description; } });
Object.defineProperty(exports, "value", { enumerable: true, get: function () { return resume_1.value; } });
Object.defineProperty(exports, "moduleDir", { enumerable: true, get: function () { return resume_1.moduleDir; } });
//# sourceMappingURL=index.js.map