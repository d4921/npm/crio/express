import prompts from 'prompts'
import path from 'path'

import { IModuleCliConfig, } from '@crio/main/types'
import { baseDir, options } from '@crio/main/cli'

import { IExpressCreateProjectResponse, IExpressInstallProjectParams, IExpressConfig, } from './types'
import { value, moduleDir,  } from './resume'

export { title, description, value, moduleDir, } from './resume'

export async function create (): Promise<IExpressCreateProjectResponse> {
  return await prompts([
    {
      type: 'number',
      name: 'serverPort',
      message: 'Which port will the server use?',
      initial: 3000,
    },
    {
      type: 'toggle',
      name: 'serverCors',
      message: 'Enable CORS on the server?',
      initial: true,
      active: 'Yes',
      inactive: 'No',
    },
    {
      type: 'toggle',
      name: 'serverBody',
      message: 'Make JSON available from the data sent by the request body to the server?',
      initial: true,
      active: 'Yes',
      inactive: 'No',
    },
    {
      type: 'toggle',
      name: 'serverQueryString',
      message: 'Make JSON available from the data sent by the request URL to the server (querystring)?',
      initial: true,
      active: 'Yes',
      inactive: 'No',
    },
    {
      type: 'text',
      name: 'serverRequestLimit',
      message: 'Maximum size of the body of requests to the server?',
      initial: '100kb',
    },
    {
      type: 'number',
      name: 'serverParameterLimit',
      message: 'Maximum number of parameters allowed in URL encoded data?',
      initial: 1000,
    },
    {
      type: 'list',
      name: 'serverStaticFolders',
      message: 'What will the server\'s static file directories be? (separate by comma)',
    },
  ])
}

export async function prepareInstall (params: IExpressInstallProjectParams): Promise<IModuleCliConfig> {
  const { projectName, responses, } = params

  responses.serverStaticFolders = responses.serverStaticFolders.filter(i => i.trim() !== '')
  
  const projectDir: string = path.join(baseDir, projectName)
  const dependencies: string[] = [ 'path', 'express', ]
  const linkDependencies: string[] = []
  const globalDependencies: string[] = []
  const createDir: string[] = responses.serverStaticFolders.map(folder => path.join('server', folder))

  const config: IExpressConfig = {
    EXPRESS_PORT: responses.serverPort,
    EXPRESS_BODY: responses.serverBody,
    EXPRESS_CORS: responses.serverCors,
    EXPRESS_PARAMETER_LIMIT: responses.serverParameterLimit,
    EXPRESS_REQUEST_LIMIT: responses.serverRequestLimit,
    EXPRESS_SERVER_QUERYSTRING: responses.serverQueryString,
    EXPRESS_STATIC_FOLDERS: responses.serverStaticFolders
  }
  const copy: [string, string][] = [
    [ 'server', 'server', ],
  ]
  
  if (responses.serverCors) {
    dependencies.push('cors')
  }
  
  dependencies.push(options.dev ? moduleDir : '@crio/express')

  return {
    projectDir,
    dependencies,
    linkDependencies,
    globalDependencies,
    createDir,
    config,
    copy,
    moduleDir,
    moduleName: value
  }
}