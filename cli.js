"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.prepareInstall = exports.create = exports.moduleDir = exports.value = exports.description = exports.title = void 0;
var prompts_1 = __importDefault(require("prompts"));
var path_1 = __importDefault(require("path"));
var cli_1 = require("@crio/main/cli");
var resume_1 = require("./resume");
var resume_2 = require("./resume");
Object.defineProperty(exports, "title", { enumerable: true, get: function () { return resume_2.title; } });
Object.defineProperty(exports, "description", { enumerable: true, get: function () { return resume_2.description; } });
Object.defineProperty(exports, "value", { enumerable: true, get: function () { return resume_2.value; } });
Object.defineProperty(exports, "moduleDir", { enumerable: true, get: function () { return resume_2.moduleDir; } });
function create() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, (0, prompts_1.default)([
                        {
                            type: 'number',
                            name: 'serverPort',
                            message: 'Which port will the server use?',
                            initial: 3000,
                        },
                        {
                            type: 'toggle',
                            name: 'serverCors',
                            message: 'Enable CORS on the server?',
                            initial: true,
                            active: 'Yes',
                            inactive: 'No',
                        },
                        {
                            type: 'toggle',
                            name: 'serverBody',
                            message: 'Make JSON available from the data sent by the request body to the server?',
                            initial: true,
                            active: 'Yes',
                            inactive: 'No',
                        },
                        {
                            type: 'toggle',
                            name: 'serverQueryString',
                            message: 'Make JSON available from the data sent by the request URL to the server (querystring)?',
                            initial: true,
                            active: 'Yes',
                            inactive: 'No',
                        },
                        {
                            type: 'text',
                            name: 'serverRequestLimit',
                            message: 'Maximum size of the body of requests to the server?',
                            initial: '100kb',
                        },
                        {
                            type: 'number',
                            name: 'serverParameterLimit',
                            message: 'Maximum number of parameters allowed in URL encoded data?',
                            initial: 1000,
                        },
                        {
                            type: 'list',
                            name: 'serverStaticFolders',
                            message: 'What will the server\'s static file directories be? (separate by comma)',
                        },
                    ])];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.create = create;
function prepareInstall(params) {
    return __awaiter(this, void 0, void 0, function () {
        var projectName, responses, projectDir, dependencies, linkDependencies, globalDependencies, createDir, config, copy;
        return __generator(this, function (_a) {
            projectName = params.projectName, responses = params.responses;
            responses.serverStaticFolders = responses.serverStaticFolders.filter(function (i) { return i.trim() !== ''; });
            projectDir = path_1.default.join(cli_1.baseDir, projectName);
            dependencies = ['path', 'express',];
            linkDependencies = [];
            globalDependencies = [];
            createDir = responses.serverStaticFolders.map(function (folder) { return path_1.default.join('server', folder); });
            config = {
                EXPRESS_PORT: responses.serverPort,
                EXPRESS_BODY: responses.serverBody,
                EXPRESS_CORS: responses.serverCors,
                EXPRESS_PARAMETER_LIMIT: responses.serverParameterLimit,
                EXPRESS_REQUEST_LIMIT: responses.serverRequestLimit,
                EXPRESS_SERVER_QUERYSTRING: responses.serverQueryString,
                EXPRESS_STATIC_FOLDERS: responses.serverStaticFolders
            };
            copy = [
                ['server', 'server',],
            ];
            if (responses.serverCors) {
                dependencies.push('cors');
            }
            dependencies.push(cli_1.options.dev ? resume_1.moduleDir : '@crio/express');
            return [2 /*return*/, {
                    projectDir: projectDir,
                    dependencies: dependencies,
                    linkDependencies: linkDependencies,
                    globalDependencies: globalDependencies,
                    createDir: createDir,
                    config: config,
                    copy: copy,
                    moduleDir: resume_1.moduleDir,
                    moduleName: resume_1.value
                }];
        });
    });
}
exports.prepareInstall = prepareInstall;
//# sourceMappingURL=cli.js.map